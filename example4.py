# can be passed as arguments to other functions
shout = lambda  text: text.upper()
whisper = lambda  text: text.lower()

def greet(func):
    # storing the function in a variable
    greeting = func("""Hi, I am created by a function passed as an argument.""")
    print(greeting)


greet(shout)
greet(whisper)