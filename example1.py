def func(val=None):
    print("I am func({})!".format(val))
    return "val -> {}".format(val)


def func2():
    return func


print("cat", func, 42)
print("cat", func(42))
print("--------------")

objects = ["cat", func]
print(objects)
print(objects[1])
print(objects[1](42))
print("--------------")

dictionary = {"cat": 1, func: 2, "func": func}  # Then: try to create dict func2
print("func is ", dictionary[func])
dictionary[func] = lambda a : a + 10
print(dictionary[func])
print("Result of func() is ", dictionary[func](1))
print(dictionary["func"](42))
print("--------------")


