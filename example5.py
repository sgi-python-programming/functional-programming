def print_label(func):
    def get_func_name():
        print("**function {} called".format(func.__name__))
        func()

    return get_func_name


@print_label
def hello():
    print("Hello python!")


hello()