from time import sleep


def callback_func(m):
    print('The message is', m)

def print_delay(message, callback):
    print("print_delay() called")
    sleep(3)
    callback(message)


print_delay("Python!", callback_func)