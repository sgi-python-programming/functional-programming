# lambda <parameter_list>: <expression>
reverse = lambda s: s[::-1]  # Violate PEP8: E731
result = reverse("katak malam")
print(result)
print('--------------')

# Or you can use Python REPL like this:
# >>> lambda s: s[::-1]
# <function <lambda> at 0x...>
# >>> _("katak malam")
# malam katak

# Or, in one-liner style
print((lambda s: s[::-1])("katak malam"))
print('--------------')
# or
(lambda s: print(s[::-1]))("katak malam")

# Note: Immediate invocation is not recommended
